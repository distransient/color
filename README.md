# Color

[CSI-SGR][] sequences for [ysh/Oil][ysh]. 

Copy the file `color.ysh` somewhere and then use the `source` command to gain access to its functions.

When ysh has full typed functions support this library will change to use those instead of procs.

[CSI-SGR]: https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_(Select_Graphic_Rendition)_parameters
[ysh]: https://www.oilshell.org/