### Provides CSI SGR sequences for colorizing terminal output.

module kel/color || return 0

proc color_csi(@attributes) {
    write --end '' -- $[$'\x1b[' ++ join(attributes, ';') ++ 'm']
}

proc color_reset_all()       { color_csi   0            }
proc color_bold()            { color_csi   1            }
proc color_dim()             { color_csi   2            }
proc color_underline()       { color_csi   4            }
proc color_invert()          { color_csi   7            }
proc color_hidden()          { color_csi   8            }
proc color_strike()          { color_csi   9            }
proc color_reset_intensity() { color_csi  22            }
proc color_reset_underline() { color_csi  24            }
proc color_reset_invert()    { color_csi  27            }
proc color_reset_hidden()    { color_csi  28            }
proc color_reset_strike()    { color_csi  29            }
proc color_fg_black()        { color_csi  30            }
proc color_fg_red()          { color_csi  31            }
proc color_fg_green()        { color_csi  32            }
proc color_fg_yellow()       { color_csi  33            }
proc color_fg_blue()         { color_csi  34            }
proc color_fg_magenta()      { color_csi  35            }
proc color_fg_cyan()         { color_csi  36            }
proc color_fg_white()        { color_csi  37            }
proc color_fg_256(n)         { color_csi  38 5 $n       }
proc color_fg_24(r, g, b)    { color_csi  38 2 $r $g $b }
proc color_reset_fg()        { color_csi  39            }
proc color_bg_black()        { color_csi  40            }
proc color_bg_red()          { color_csi  41            }
proc color_bg_green()        { color_csi  42            }
proc color_bg_yellow()       { color_csi  43            }
proc color_bg_blue()         { color_csi  44            }
proc color_bg_magenta()      { color_csi  45            }
proc color_bg_cyan()         { color_csi  46            }
proc color_bg_white()        { color_csi  47            }
proc color_bg_256(n)         { color_csi  48 5 $n       }
proc color_bg_24(r, g, b)    { color_csi  48 2 $r $g $b }
proc color_reset_bg()        { color_csi  49            }

proc color_ize(msg, sequence) {
    write --end '' -- $[sequence ++ msg ++ $(color_reset_all)]
}

proc color_ize_bold(msg)           { color_ize $msg $(color_bold)           }
proc color_ize_dim(msg)            { color_ize $msg $(color_dim)            }
proc color_ize_underline(msg)      { color_ize $msg $(color_underline)      }
proc color_ize_invert(msg)         { color_ize $msg $(color_invert)         }
proc color_ize_hidden(msg)         { color_ize $msg $(color_hidden)         }
proc color_ize_strike(msg)         { color_ize $msg $(color_strike)         }
proc color_ize_black(msg)          { color_ize $msg $(color_fg_black)       }
proc color_ize_red(msg)            { color_ize $msg $(color_fg_red)         }
proc color_ize_green(msg)          { color_ize $msg $(color_fg_green)       }
proc color_ize_yellow(msg)         { color_ize $msg $(color_fg_yellow)      }
proc color_ize_blue(msg)           { color_ize $msg $(color_fg_blue)        }
proc color_ize_magenta(msg)        { color_ize $msg $(color_fg_magenta)     }
proc color_ize_cyan(msg)           { color_ize $msg $(color_fg_cyan)        }
proc color_ize_white(msg)          { color_ize $msg $(color_fg_white)       }
proc color_ize_256(msg, n)         { color_ize $msg $(color_fg_256 $n)      }
proc color_ize_24(msg, r, g, b)    { color_ize $msg $(color_fg_24 $r $g $b) }
proc color_ize_bg_black(msg)       { color_ize $msg $(color_bg_black)       }
proc color_ize_bg_red(msg)         { color_ize $msg $(color_bg_red)         }
proc color_ize_bg_green(msg)       { color_ize $msg $(color_bg_green)       }
proc color_ize_bg_yellow(msg)      { color_ize $msg $(color_bg_yellow)      }
proc color_ize_bg_blue(msg)        { color_ize $msg $(color_bg_blue)        }
proc color_ize_bg_magenta(msg)     { color_ize $msg $(color_bg_magenta)     }
proc color_ize_bg_cyan(msg)        { color_ize $msg $(color_bg_cyan)        }
proc color_ize_bg_white(msg)       { color_ize $msg $(color_bg_white)       }
proc color_ize_bg_256(msg, n)      { color_ize $msg $(color_bg_256 $n)      }
proc color_ize_bg_24(msg, r, g, b) { color_ize $msg $(color_bg_24 $r $g $b) }