# The Made Up License For Non-Evil Use

I am definitely not a lawyer but I've gotta say if you feel like using or 
adapting this software for any non-commercial, non-evil activity, please be my 
guest, new friend. Let me know what you think, and if you adapt it, please 
make it publicly available and include this license file unaltered.

Just know I'm sharing this software without any attached warranty, liability, 
or guarantee of support. That means I owe you nothing here. Don't get mad if my 
software breaks your shit because preventing that is your task, not mine.

If you want to use this in your "not-evil" business and you don't ask me to 
purchase the (modestly-priced) license to do so then I will probably sue you.

Evil activities excluding you from legal use of this software include:

- Policing
- Imperialism
- Crimes Against Humanity
- Cruelty to people or the environment

These are only examples, however. It is exclusively within my own judgement if 
an activity classifies as evil or not.

If you wish, you can ask me if I think your use-case is evil, but if you really
feel like you have to ask the answer will probably be "stop being evil".

But don't ask me to change the license. Won't happen.

Copy "right" Kel K. 
Email's in the commits.